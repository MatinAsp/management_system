# management_system

This is a Django app for managing companies and their employers and employees.

## Setup

First, clone the project and create an virtual environment for it. Then run this command to  install the dependencies:

```sh
$ pip install -r requirements.txt
```

Then run this commands for setting up the database:

```sh
$ python manage.py makemigrations
$ python manage.py migrate
```

Now you can run the server and create a superuser and login to admin site.

## Requests

To see what requests you can send, import the "requests.postman_collection.json" file with the Postman app and see the examples.