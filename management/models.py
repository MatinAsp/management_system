from django.db import models
from django.contrib.auth.models import AbstractUser


class Company(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=True, verbose_name='ID')
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name


class Account(AbstractUser):
    company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, default=None)
    is_employer = models.BooleanField(default=False)


class Task(models.Model):
    assignee = models.ForeignKey(Account, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    deadline = models.DateTimeField()

    def __str__(self):
        return self.title


class CompanyRequest(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return self.company.name


class EmployerRequest(CompanyRequest):
    pass


class EmployeeRequest(CompanyRequest):
    pass
