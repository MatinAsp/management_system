from django.urls import path, include
from .views import (register, login, employer_request, employee_request, employer_answer_request,
                    employee_answer_request, create_task, CompanyViewSet, TaskViewSet, EmployerRequestViewSet,
                    EmployeeRequestViewSet, EmployeeViewSet, remove_employee, AccountViewSet, create_company)
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'companies', CompanyViewSet)
router.register(r'employee_requests', EmployeeRequestViewSet)
router.register(r'employer_requests', EmployerRequestViewSet)
router.register(r'tasks', TaskViewSet)
router.register(r'employees', EmployeeViewSet)
router.register(r'accounts', AccountViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('register', register, name='register'),
    path('login', login, name='login'),
    path('create_company', create_company, name='create_company'),
    path('employer_request/<slug:company_id>', employer_request, name='employer_request'),
    path('employee_request/<slug:company_id>', employee_request, name='employee_request'),
    path('employee_answer_request/<slug:request_id>', employee_answer_request, name='employee_answer_request'),
    path('employer_answer_request/<slug:request_id>', employer_answer_request, name='employer_answer_request'),
    path('remove_employee/<slug:username>', remove_employee, name='remove_employee'),
    path('create_task', create_task, name='create_task'),
]
