from django.contrib import auth
from .models import Account, Company, EmployeeRequest, EmployerRequest, Task
from django.http import JsonResponse
from datetime import datetime
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import (AccountSerializer, CompanySerializer, EmployeeRequestSerializer, EmployerRequestSerializer,
                          TaskSerializer)


def register(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        Account.objects.create_user(username=username, password=password, email=email)
        return JsonResponse({'status': 'success'}, status=200)
    return JsonResponse({'status': 'fail'}, status=400)


def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(request, username=username, password=password)
        if user:
            auth.login(request, user)
            return JsonResponse({'status': 'success'}, status=200)
        else:
            return JsonResponse({'status': 'fail', 'message': 'wrong username or password'}, status=400)
    return JsonResponse({'status': 'fail'}, status=400)


def employer_request(request, company_id):
    if request.method == 'POST':
        if not request.user.is_authenticated:
            return JsonResponse({'status': 'fail', 'message': 'authentication error'}, status=401)
        user = Account.objects.get(username=request.user.username)
        if not (user.company is None):
            return JsonResponse({'status': 'fail', 'message': 'already has a company'}, status=400)
        try:
            company = Company.objects.get(pk=company_id)
        except:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        EmployerRequest.objects.create(user=user, company=company)
        return JsonResponse({'status': 'success'}, status=200)
    return JsonResponse({'status': 'fail'}, status=400)


def employee_request(request, company_id):
    if request.method == 'POST':
        if not request.user.is_authenticated:
            return JsonResponse({'status': 'fail', 'message': 'authentication error'}, status=401)
        user = Account.objects.get(username=request.user.username)
        if not (user.company is None):
            return JsonResponse({'status': 'fail', 'message': 'already has a company'}, status=400)
        try:
            company = Company.objects.get(pk=company_id)
        except:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        user = request.user
        EmployeeRequest.objects.create(user=user, company=company)
        return JsonResponse({'status': 'success'}, status=200)
    return JsonResponse({'status': 'fail'}, status=400)


def employee_answer_request(request, request_id):
    if request.method == 'POST':
        if not request.user.is_authenticated:
            return JsonResponse({'status': 'fail', 'message': 'authentication error'}, status=401)
        user = Account.objects.get(username=request.user.username)
        try:
            employee_request = EmployeeRequest.objects.get(pk=request_id)
        except:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        if user.company.id != employee_request.company.id or not user.is_employer:
            return JsonResponse({'status': 'fail', 'message': 'permission denied'}, status=400)
        answer = request.POST.get('answer')
        if answer is None:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        employee_request.delete()
        if not (employee_request.user.company is None):
            return JsonResponse({'status': 'fail', 'message': 'already has a company'}, status=400)
        if answer:
            employee_request.user.company = employee_request.company
            employee_request.user.save()
        return JsonResponse({'status': 'success'}, status=200)
    return JsonResponse({'status': 'fail'}, status=400)


def employer_answer_request(request, request_id):
    if request.method == 'POST':
        if not (request.user.is_authenticated and request.user.is_superuser):
            return JsonResponse({'status': 'fail', 'message': 'authentication error'}, status=401)
        user = Account.objects.get(username=request.user.username)
        try:
            employer_request = EmployerRequest.objects.get(pk=request_id)
        except:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        if not user.is_superuser:
            return JsonResponse({'status': 'fail', 'message': 'permission denied'}, status=400)
        answer = request.POST.get('answer')
        if answer is None:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        employer_request.delete()
        if (not (employer_request.user.company is None)) and employer_request.user.company.id != employer_request.company.id:
            return JsonResponse({'status': 'fail', 'message': 'already has a company'}, status=400)
        if answer:
            employer_request.user.company = employer_request.company
            employer_request.user.is_employer = True
            employer_request.user.save()
        return JsonResponse({'status': 'success'}, status=200)
    return JsonResponse({'status': 'fail'}, status=400)


def create_task(request):
    if request.method == 'POST':
        if not request.user.is_authenticated:
            return JsonResponse({'status': 'fail', 'message': 'authentication error'}, status=401)
        user = Account.objects.get(username=request.user.username)
        if not user.is_employer:
            return JsonResponse({'status': 'fail', 'message': 'permission denied'}, status=400)
        try:
            assignee = Account.objects.get(username=request.POST.get('assignee_username'))
            title = request.POST.get('title')
            description = request.POST.get('description')
            deadline = request.POST.get('deadline')
        except:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        if (assignee is None) or (title is None) or (deadline is None) or (deadline is None):
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        Task.objects.create(assignee=assignee, title=title, description=description, deadline=datetime.utcfromtimestamp(int(deadline)))
        return JsonResponse({'status': 'success'}, status=200)
    return JsonResponse({'status': 'fail'}, status=400)


def remove_employee(request, username):
    if request.method == 'POST':
        if not request.user.is_authenticated:
            return JsonResponse({'status': 'fail', 'message': 'authentication error'}, status=401)
        try:
            employee = Account.objects.get(username=username)
            user = Account.objects.get(username=request.user.username)
        except:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        if (user.company and user.is_employer and employee.company and employee.company.id == user.company.id and
                (not employee.is_employer)):
            employee.company = None
            employee.save()
            return JsonResponse({'status': 'success'}, status=200)
        return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
    return JsonResponse({'status': 'fail'}, status=400)


def create_company(request):
    if request.method == 'POST':
        if not (request.user.is_authenticated and request.user.is_superuser):
            return JsonResponse({'status': 'fail', 'message': 'authentication error'}, status=401)
        company_name = request.POST.get('company_name')
        if company_name is None:
            return JsonResponse({'status': 'fail', 'message': 'invalid request'}, status=400)
        Company.objects.create(name=company_name)
        return JsonResponse({'status': 'success'}, status=200)
    return JsonResponse({'status': 'fail'}, status=400)


class CompanyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = [permissions.AllowAny]


class IsEmployer(permissions.BasePermission):

    def has_permission(self, request, view):
        if not bool(request.user and request.user.is_authenticated):
            return False
        account = Account.objects.get(username=request.user.username)
        return account.is_employer


class TaskViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Task.objects.all()
    permission_classes = [IsEmployer]

    def list(self, request):
        queryset = Task.objects.all()
        tasks = []
        account = Account.objects.get(username=request.user.username)
        for query in queryset:
            if query.assignee.company.id == account.company.id:
                tasks.append(query.id)
        queryset = Task.objects.filter(pk__in=tasks).order_by('deadline')
        serializer = TaskSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


class EmployerRequestViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = EmployerRequest.objects.all()
    serializer_class = EmployerRequestSerializer
    permission_classes = [permissions.IsAdminUser]


class EmployeeRequestViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = EmployeeRequest.objects.all()
    permission_classes = [IsEmployer]

    def list(self, request):
        queryset = EmployeeRequest.objects.all()
        requests = []
        account = Account.objects.get(username=request.user.username)
        for query in queryset:
            if query.company.id == account.company.id:
                requests.append(query.id)
        queryset = EmployeeRequest.objects.filter(pk__in=requests)

        serializer = EmployeeRequestSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


class EmployeeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Account.objects.all()
    permission_classes = [IsEmployer]

    def list(self, request):
        queryset = Account.objects.all()
        accounts = []
        account = Account.objects.get(username=request.user.username)
        for query in queryset:
            if query.company and query.company.id == account.company.id:
                accounts.append(query.id)
        queryset = Account.objects.filter(pk__in=accounts)
        serializer = AccountSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


class AccountViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    permission_classes = [permissions.IsAdminUser]
