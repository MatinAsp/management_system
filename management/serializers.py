from rest_framework import serializers
from .models import Account, Company, Task, CompanyRequest, EmployeeRequest, EmployerRequest


class AccountSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Account
        fields = '__all__'


class CompanySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Company
        fields = '__all__'


class TaskSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Task
        fields = '__all__'


class CompanyRequestSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = CompanyRequest
        fields = '__all__'


class EmployerRequestSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = EmployerRequest
        fields = '__all__'


class EmployeeRequestSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = EmployeeRequest
        fields = '__all__'
