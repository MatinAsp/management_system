from django.contrib import admin
from .models import *


admin.site.register(Account)
admin.site.register(Company)
admin.site.register(Task)
admin.site.register(EmployerRequest)
admin.site.register(EmployeeRequest)
